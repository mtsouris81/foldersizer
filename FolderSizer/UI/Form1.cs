﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace FolderSizer
{
    public partial class Form1 : Form
    {
        #region Fields
        string startPath = @"C:\Users";
        List<FolderSizeInfo> Result;
        TreeNode RootNode = null;
        ProgressForm progressDisplay = null;
        List<FolderSizeInfo> AssignedParents = new List<FolderSizeInfo>();
        double counter = 0;
        double count = 0;
        double ratio = 0;
        string CurrentWorkStep = string.Empty;
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        #region UI Event Handlers
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressDisplay.SetProgress(e.ProgressPercentage, e.UserState.ToString());
        }
        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            foreach (var n in Result)
            {
                BuildTreeNodes(n, RootNode);
            }

            this.progressDisplay.Hide();
            RootNode.Expand();
            this.Focus();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.startPath = this.folderBrowserDialog1.SelectedPath;
                StartWork();
            }
        }
        #endregion

        #region Methods
        protected void IncrementThreadProgress()
        {
            counter++;
            if (counter == 0 || count == 0)
                ratio = 0;
            else
                ratio = counter / count;
            this.backgroundWorker1.ReportProgress((int)(ratio * 100), CurrentWorkStep);
        }

        protected void StartWorkAsynch(object sender, DoWorkEventArgs e)
        {
            AssignedParents.Clear();
            List<FolderSizeInfo> folderSizes = new List<FolderSizeInfo>();

            CurrentWorkStep = "finding all folders";
            IncrementThreadProgress();

            string[] folders = FolderEnumerator.EnumerateFoldersRecursively(startPath).ToArray(); // Directory.GetDirectories(startPath, "*", SearchOption.AllDirectories);

            counter = 0;
            count = folders.Length;
            ratio = 0;

            CurrentWorkStep = "Determine Folder Local Sizes " + folders.Length.ToString();
            foreach (var f in folders)
            {
                IncrementThreadProgress();
                long size = DetermineFolderLocalSize(f);
                FolderSizeInfo sizeInfo = new FolderSizeInfo()
                {
                    LocalSize = size,
                    Path = f
                };
                folderSizes.Add(sizeInfo);
            }


            CurrentWorkStep = "Assign Parent Folders " + count.ToString();
            count = folderSizes.Count;
            counter = 0;
            foreach (var s in folderSizes)
            {
                IncrementThreadProgress();
                FindParentFolder(s, folderSizes);
            }

            Result = folderSizes.Where(x => !AssignedParents.Contains(x)).ToList();


            CurrentWorkStep = "Determine Accumulated Size " + folderSizes.Count.ToString();
            counter = 0;
            count = folderSizes.Count;
            foreach (var s in folderSizes)
            {
                IncrementThreadProgress();
                DetermineAccumulatedSize(s);
            }

            CurrentWorkStep = "Sorting Results";
            counter = 0;
            foreach (var s in folderSizes)
            {
                IncrementThreadProgress();
                s.ChildFolders = s.ChildFolders.OrderByDescending(x => x.AccumulativeSize).ToList();
            }


            Result = folderSizes.Where(x => !AssignedParents.Contains(x)).OrderByDescending(x => x.AccumulativeSize).ToList();

        }

        protected void StartWork()
        {
            treeView1.Nodes.Clear();
            this.RootNode = this.treeView1.Nodes.Add("ROOT");
            progressDisplay.Show();
            progressDisplay.SetProgress(0, 0, "searching");
            this.backgroundWorker1.RunWorkerAsync(null);
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            progressDisplay = new ProgressForm();

            
            this.backgroundWorker1.DoWork += StartWorkAsynch;
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);

            if (this.folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.startPath = this.folderBrowserDialog1.SelectedPath;
                StartWork();
            }
            else
            {
                this.Close();
            }

        }

        protected void DetermineAccumulatedSize(FolderSizeInfo s)
        {
            long size = 0;
            CrawlChildFolders(s, ref size);
            s.AccumulativeSize = size;
        }

        protected void CrawlChildFolders(FolderSizeInfo folder, ref long size)
        {
            size += folder.LocalSize;
            foreach (var c in folder.ChildFolders)
            {
                CrawlChildFolders(c, ref size);
            }
        }

        protected void BuildTreeNodes(FolderSizeInfo folder, TreeNode node)
        {
            string pathDisplay = folder.Path.Substring(folder.Path.LastIndexOf("\\") + 1);
            string sizeUnit = folder.TotalSizeGigabytes >= 1 ? "GB" : "mb";
            double sizeNumber = folder.TotalSizeGigabytes >= 1 ? folder.TotalSizeGigabytes : folder.TotalSizeMegabytes;

            TreeNode newNode = node.Nodes.Add(string.Format("{0}   [ {1:0.00} {2} ]", pathDisplay, sizeNumber, sizeUnit));
            newNode.Tag = folder.Path;

            foreach (var c in folder.ChildFolders)
            {
                BuildTreeNodes(c, newNode);
            }
        }

        protected void FindParentFolder(FolderSizeInfo s, List<FolderSizeInfo> folderSizes)
        {
            string parentFolder = s.Path.Substring(0, s.Path.LastIndexOf("\\"));
            FolderSizeInfo found = folderSizes.Where(x => x.Path.Equals(parentFolder, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            if (found != null)
            {
                found.ChildFolders.Add(s);
                if (!AssignedParents.Contains(s))
                {
                    AssignedParents.Add(s);
                }
            }
        }

        protected long DetermineFolderLocalSize(string f)
        {
            long size = 0;
            string[] files = new string[0];
            try
            {
               files = Directory.GetFiles(f);
            }
            catch { }

            size = 0;
            
            foreach (var file in files)
            {
                try
                {
                    FileInfo info = new FileInfo(file);
                    size += info.Length;
                }
                catch { }
            }

            return size;
        }
        #endregion

        private void goToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectedFolder = treeView1.SelectedNode.Tag as string;
            ProcessStartInfo openFolder = new ProcessStartInfo();
            openFolder.FileName = selectedFolder;
            Process.Start(openFolder);
        }
    }
}
