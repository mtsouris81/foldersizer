﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FolderSizer
{
    public partial class ProgressForm : Form
    {
        public ProgressForm()
        {
            InitializeComponent();
        }

        public void SetProgress(int progress, string message)
        {

            if (progress > progressBar1.Maximum)
            {
                progress = progressBar1.Maximum;
            }

            progressBar1.Value = progress;
            this.Text = message;

        }
        public void SetProgress(int count, int total, string message)
        {
            float value = 0;

            if (count == 0)
            {
                value = 0;
            }
            else
            {
                if (count > total)
                {
                    value = total;
                }
                else
                {
                    value = ((float)count / (float)total) * 100.0f;
                }
            }

            if (value > progressBar1.Maximum)
            {
                value = progressBar1.Maximum;
            }

            progressBar1.Value = (int)value;
            this.Text = message;
        }
    }
}
