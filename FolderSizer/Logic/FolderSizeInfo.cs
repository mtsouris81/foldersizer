﻿using System.Collections.Generic;

namespace FolderSizer
{
    public class FolderSizeInfo
    {
        public FolderSizeInfo()
        {
            ChildFolders = new List<FolderSizeInfo>();
        }
        public long LocalSize { get; set; }
        public long AccumulativeSize { get; set; }
        public string Path { get; set; }
        public List<FolderSizeInfo> ChildFolders { get; set; }

        public double TotalSizeMegabytes
        {
            get
            {
                if (AccumulativeSize == 0)
                    return 0;

                return (AccumulativeSize / 1024f) / 1024f;
            }
        }
        public double TotalSizeGigabytes
        {
            get
            {
                if (AccumulativeSize == 0)
                    return 0;

                return ((AccumulativeSize / 1024f) / 1024f) / 1024f;
            }
        }
    }
}
